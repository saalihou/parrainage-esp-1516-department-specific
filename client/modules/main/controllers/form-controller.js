var app = angular.module('parrainage-esp-1516-main');

app.controller('FormController', function ($scope, Godfathers, Godsons) {
    $scope.godfathers = Godfathers.query();
    $scope.godsons = Godsons.query();
});