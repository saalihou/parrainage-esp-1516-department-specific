var app = angular.module('parrainage-esp-1516-main');

app.controller('MatchingBoxController',
  function ($scope, $meteor, Godfathers, Godsons) {
    $scope.godsons = Godsons.query();
    $scope.godfathers = Godfathers.query();

    function updateEligibleGodsonsCount() {
      $meteor.call('countEligibleGodsons')
        .then(function (count) {
          $scope.eligibleGodsonsCount = count;
        });
    }
    updateEligibleGodsonsCount();

    $scope.pickGodson = function () {
      $meteor.call('getEligibleGodson')
        .then(function (godsonId) {
          $scope.pickedGodson = _.find($scope.godsons,
            function (godson) {
              return godson._id === godsonId;
            }
          );
        });
    }

    $scope.match = function (godson) {
      $meteor.call('match', godson._id)
        .then(function (matchedGodfatherId) {
          $scope.matchedGodfather = _.find($scope.godfathers,
            function (godfather) {
              return godfather._id === matchedGodfatherId;
            }
          );
          updateEligibleGodsonsCount();
        }, function (error) {
          console.log(error);
        });
    }

    $scope.reset = function () {
      $scope.pickedGodson = undefined;
      $scope.matchedGodfather = undefined;
    }

    $scope.needsGodfather = function (godson) {
      return !godson.godFather;
    }
  }
);
