var app = angular.module('parrainage-esp-1516-main');

app.controller('SponsoringController',
  function ($scope, Godfathers, Godsons, pespDepartmentFilter) {
    $scope.godsons = Godsons.query();

    $scope.searchFn = function (godson) {
      if (!$scope.search || $scope.search.length === 0) {
        return true;
      }
      var testedVal = godson.firstName + godson.lastName;
      testedVal += pespDepartmentFilter(godson.department);
      testedVal += godson.godFather.firstName + godson.godFather.lastName;
      testedVal += pespDepartmentFilter(godson.godFather.department);

      testedVal = testedVal.replace(/\s/g, '')
        .replace(/[éèêë]/ig, 'e')
        .replace(/[ïî]/ig, 'i')
        .replace(/[öô]/ig, 'o');

      var search = $scope.search.replace(/\s/g, '');
      var searchRegex = new RegExp(search, 'i');
      return searchRegex.test(testedVal);
    }
  }
);
