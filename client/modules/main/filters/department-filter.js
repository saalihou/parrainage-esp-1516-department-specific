var app = angular.module('parrainage-esp-1516-main');

app.filter('pespDepartment', function () {
  return function (input) {
    var output;
    switch (input) {
      case 'i':
        output = 'informatique'
        break;
      case 'tr':
        output = 'télécoms réseaux'
        break;
      case 'e':
        output = 'électrique'
        break;
      case 'm':
        output = 'mécanique'
        break;
      case 'gcba':
        output = 'GCBA'
        break;
      case 'g':
        output = 'gestion'
        break;
      case 'c':
        output = 'civil'
        break;
    }
    return output;
  }
});
