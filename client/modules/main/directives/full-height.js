var app = angular.module('parrainage-esp-1516-main');

app.directive('pespFullHeight',
  function ($window) {
    function link(scope, element, attrs) {
      function apply() {
        element.css('height', angular.element($window).height());
      }
      apply();
      angular.element($window).on('resize', apply);
    }
    return {
      link: link
    }
  }
);
