var app = angular.module('parrainage-esp-1516-main');

app.directive('pespCollectionForm',
    function () {
        return {
            templateUrl: 'client/modules/main/views/collection-form-template.html',
            controller: function ($scope) {
                $scope.pagination = {
                    page: 1,
                    limit: 5
                };
                $scope.save = function (item) {
                    $scope.collection.save(item).then(function () {
                        $scope.newItem={};
                        $scope.pagination.page = Math.ceil($scope.collection.length / $scope.pagination.limit);
                    });
                };

                $scope.remove = function (item) {
                    $scope.collection.remove(item);
                };

                $scope.searchFn = function (godson) {
                    if (!$scope.search || $scope.search.length === 0) {
                        return true;
                    }
                    var testedVal = godson.firstName + godson.lastName;

                    testedVal = testedVal.replace(/\s/g, '')
                        .replace(/[éèêë]/ig, 'e')
                        .replace(/[ïî]/ig, 'i')
                        .replace(/[öô]/ig, 'o');

                    var search = $scope.search.replace(/\s/g, '');
                    var searchRegex = new RegExp(search, 'i');
                    return searchRegex.test(testedVal);
                }
            },
            scope: {
                collection: '=',
                name: '='
            }
        }
    }
);
