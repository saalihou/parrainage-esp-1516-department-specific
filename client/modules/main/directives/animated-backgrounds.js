var app = angular.module('parrainage-esp-1516-main');

app.directive('pespAnimatedBackgrounds',
  function ($interval, $animate) {
    function link(scope, element, attrs) {
      var interval = parseInt(attrs.pespAnimatedBackgroundsInterval) || 1000;

      var urls = ['bg-1.jpg', 'bg-2.jpg', 'bg-3.jpg', 'bg-4.jpg', 'bg-5.jpg'];
      var backgrounds = [];
      var currentIndex = 0;

      element.css('position', 'relative');
      urls.forEach(function (url) {
        var backgroundImage = angular.element(
          '<img src="images/' + url + '"/>'
        );
        backgroundImage.css({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%',
          height: '100%',
          zIndex: 0
        }).addClass('bg-image');

        element.append(backgroundImage);
        backgrounds.push(backgroundImage);
      });
      backgrounds[0].siblings().not('.bg-image')
        .css({
          position: 'relative',
          zIndex: 10
        });

      $interval(function () {
        backgrounds.forEach(function (bg) {
          $animate.removeClass(bg, 'visible');
        });
        $animate.addClass(backgrounds[currentIndex], 'visible');
        currentIndex++;
        currentIndex = (currentIndex === urls.length) ? 0 : currentIndex;
      }, interval);
    }
    return {
      link: link
    }
  }
);
