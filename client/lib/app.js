var app = angular.module('parrainage-esp-1516-main', [
  'angular-meteor',
  'ui.router',
  'ngAnimate',
  'ngMaterial',
  'md.data.table'
]);
