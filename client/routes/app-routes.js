var app = angular.module('parrainage-esp-1516-main');

app.config(function ($stateProvider) {
  $stateProvider
    .state('home', {
        url: '/match',
        templateUrl: 'client/modules/main/views/home.html'
      })
    .state('sponsors-list', {
        url: '/',
        templateUrl: 'client/modules/main/views/sponsors-list.html'
      })
    .state('form', {
        url: '/formulaire',
        templateUrl: 'client/modules/main/views/form.html'
      });
});
