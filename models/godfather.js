Godfathers = new Mongo.Collection('godfathers');

var GodfatherSchema = new SimpleSchema({
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  godsons: {
    type: [String],
    defaultValue: []
  },
  'godsons.$': {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  }
});

Godfathers.attachSchema(GodfatherSchema);
