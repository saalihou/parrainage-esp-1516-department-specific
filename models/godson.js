Godsons = new Mongo.Collection('godsons', {
  transform: function (godson) {
    godson.godFather = Godfathers.findOne(godson.godFather);
    return godson;
  }
});

var GodsonSchema = new SimpleSchema({
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  godFather: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  }
});

Godsons.attachSchema(GodsonSchema);
