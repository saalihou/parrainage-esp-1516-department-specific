Meteor.methods({
  getEligibleGodson: function () {
    var eligibleGodsons, godson, eligibleGodsonsSize, randomIndex;

    eligibleGodsons = Godsons.find({
      godFather: {$exists: false}
    });

    // transform the cursor into an array
    eligibleGodsons = eligibleGodsons.fetch();

    randomIndex = Math.floor(Math.random() * eligibleGodsons.length);
    godson = eligibleGodsons[randomIndex];

    return godson._id;
  }
});
