Meteor.methods({
  match: function (godsonId) {
    var godson, possibleGodfathers, godfather, eligibleGodsonsSize,
    randomIndex;
    godson = Godsons.findOne(godsonId);

    eligibleGodsonsSize = 0;
    do {
      possibleGodfathers = Godfathers.find({
        godsons: {$size: eligibleGodsonsSize}
      });
      eligibleGodsonsSize++;
    } while (possibleGodfathers.count() === 0)

    // transform the cursor into an array
    possibleGodfathers = possibleGodfathers.fetch();

    randomIndex = Math.floor(Math.random() * possibleGodfathers.length);
    godfather = possibleGodfathers[randomIndex];

    if (godson.godFather) {
      Godfathers.update({_id: godson.godFather}, {
        $pull: {
          godsons: godson._id
        }
      });
    }
    
    Godfathers.update(godfather._id, {
      $push: {
        godsons: godsonId
      }
    });

    Godsons.update(godsonId, {
      $set: {
        godFather: godfather._id
      }
    });

    return godfather._id;
  }
});
